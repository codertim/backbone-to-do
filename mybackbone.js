
// alert("Starting ...");


(function($) {

	var Task = Backbone.Model.extend({
		defaults: {
			taskMessage: 'new task',
			priority: 2
		}
	});


	var TaskList = Backbone.Collection.extend({
		model: Task,
		uri: '/api/tasks'
	});


	var TasksView = Backbone.View.extend({
		el: $('#backbone-stuff'),

		events: {
			'click input[type=button]' : 'addNewTask'
		},

		initialize: function() {
			_.bindAll(this, 'render', 'addNewTask', 'addToDom');
			this.collection = new TaskList();
			this.collection.bind('add', this.addToDom);
			this.render();
		},

		render: function() {
			$(this.el).append("<h2>Backbone ToDo</h2>");
			$(this.el).append("<input type='text' />");
			var prioritySelect = "<select id='priority-select'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option></select>";
			$(this.el).append(prioritySelect);
			$(this.el).append("<input type='button' value='Add' />");
			$(this.el).append("<br /><br /><h3>Tasks:</h3><br /><div id='tasks-div'></div>");
			$(this.el).append("<br /><br />");
		},


		addToDom: function(newTask) {
			alert("addToDom - Starting ...");
			// alert("newTask.priority=" + newTask.get('priority'));
			// $('div#tasks-div', this.el).append("<div id='single-task'>" + newTask.get('taskMessage') +  " - " + newTask.get('priority') + "</div>");
			$('div#tasks-div').append("<div id='single-task'>" + newTask.get('taskMessage') +  " - " + newTask.get('priority') + "</div>");
		},

		addNewTask: function() {
			alert("addNewTask - Starting ...");
			var task = new Task();
			task.set({
				taskMessage: $("input[type=text]").val(),
				// priority: Math.floor(Math.random()*5 ) + 1
				priority: $("select#priority-select").val()
			});
			$("input[type=text]").val("");
			this.collection.add(task);
		}
	});

	var TasksView = new TasksView();
})(jQuery);


